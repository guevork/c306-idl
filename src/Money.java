public class Money implements IMoney {
	private int fAmount;
	private String fCurrency;
	
	public Money(int amount, String currency){
		fAmount = amount;
		fCurrency = currency;
	}

	public int getAmount() {
		return fAmount;
	}

	public void setAmount(int fAmount) {
		this.fAmount = fAmount;
	}

	public String getCurrency() {
		return fCurrency;
	}

	public void setCurrency(String fCurrency) {
		this.fCurrency = fCurrency;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		
		if (this == obj)
			return true;
		
		if (!(obj instanceof Money))
			return false;
		
		Money other = (Money) obj;		
		return getCurrency().equals(other.getCurrency()) && getAmount() == other.getAmount();
	}

	@Override
	public IMoney add(IMoney m) {
		return m.addMoney(this);
	}
	
	/**
	 * @param m Money 
	 * Si on additionne deux memes devises, on retourne l'addition Money, sinon on retourne un MoneyBag qui est compose des deux
	 */
	@Override
	public IMoney addMoney(Money m) {
		return (m.getCurrency().equals(getCurrency())) ? new Money(getAmount() + m.getAmount(), getCurrency()) : new MoneyBag(this, m);
	}

	/**
	 * @param mb MoneyBag
	 * On effectue l'addition depuis le MoneyBag.
	 */
	@Override
    public IMoney addMoneyBag(MoneyBag mb) {
        return mb.addMoney(this);
	}
}

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MoneyBagTest {
	private Money 	 f12CHF;
	private Money 	 f14CHF;
	private Money 	 f7USD;
	private Money 	 f21USD;
	private MoneyBag fMB1;
	private MoneyBag fMB2;
	
	@Before
	public void setUp() {
		f12CHF = new Money(12, "CHF");
		f14CHF = new Money(14, "CHF");
		f7USD  = new Money(7, "USD");
		f21USD = new Money(21, "USD");
		fMB1   = new MoneyBag(f12CHF, f7USD);
		fMB2   = new MoneyBag(f14CHF, f21USD);
	}

	@Test
	public void testBagEquals() {
		assertTrue(!fMB1.equals(null));
		assertEquals(fMB1, fMB1);
		assertTrue(!fMB1.equals(f12CHF));
		assertTrue(!f12CHF.equals(fMB1));
		assertTrue(!fMB1.equals(fMB2));
	} 
	
	@Test
	public void testMixedSimpleAdd() {
		// [12 CHF] + [7 USD] == {[12 CHF][7 USD]}
		Money bag[] = { f12CHF, f7USD };
		MoneyBag expected = new MoneyBag(bag);
		assertEquals(expected, f12CHF.add(f7USD));
	}
	
	@Test
    public void testBagSimpleAdd() {
        // {[12 CHF][7 USD]} + [14 CHF] == {[26 CHF][7 USD]}
        Money bag[] = { new Money(26, "CHF"), new Money(7, "USD") };
        MoneyBag expected = new MoneyBag(bag);
        assertEquals(expected, fMB1.add(f14CHF));
    }
    
	@Test
    public void testSimpleBagAdd() {
        // [14 CHF] + {[12 CHF][7 USD]} == {[26 CHF][7 USD]}
        Money bag[] = { new Money(26, "CHF"), new Money(7, "USD") };
        MoneyBag expected = new MoneyBag(bag);
        assertEquals(expected, f14CHF.add(fMB1));
    }

	@Test
	public void testBagBagAdd() {
		// {[12 CHF][7 USD]} + {[14 CHF][21 USD]} == {[26 CHF][28 USD]}
		Money bag[] = { new Money(26, "CHF"), new Money(28, "USD") };
        MoneyBag expected = new MoneyBag(bag);
        assertEquals(expected, fMB1.add(fMB2));
	}
	
	@Test
	public void testSimplifyAddSimple() {
		// {[12 CHF][7 USD]} + [-12 CHF] == [7 USD]
		assertEquals(f7USD, fMB1.add(new Money(-12, "CHF")));
	}
	
	@Test
	public void testSimplifyAddBag() {
		// {[12 CHF][7 USD]} + {[-12 CHF][-7 USD]} == { }
		MoneyBag expected = new MoneyBag();		
		Money[] bag = { new Money(-12, "CHF"), new Money(-7, "USD") };		
		assertEquals(expected, fMB1.add(new MoneyBag(bag)));
	}
	
	@Test
	public void testSimplifyAddBagBag() {
		// {[12 CHF][7 USD][5 EUR]} + {[-12 CHF][-5 EUR]} == [7 USD]
		Money[] resBag = { new Money(12, "CHF"), new Money(7, "USD"), new Money(5, "EUR") };
		MoneyBag fMB3 = new MoneyBag(resBag);
		
		Money[] adBag = { new Money(-12, "CHF"), new Money(-5, "EUR") };	
		MoneyBag fMB4 = new MoneyBag(adBag);
		
		assertEquals(f7USD, fMB3.add(fMB4));	
	}
}

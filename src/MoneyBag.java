import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

public class MoneyBag implements IMoney {
	private Vector<Money> fMonies = new Vector<Money>();
	
	/**
	 * Constructeur par d�faut
	 * Construit un MoneyBag vide
	 */
	MoneyBag() {}
	
	/**
	 * @param m1 Money
	 * @param m2 Money
	 * Construit un MoneyBag en regroupant m1 et m2
	 */
	MoneyBag(Money m1, Money m2){
		appendMoney(m1);
		appendMoney(m2);
	}
	
	/**
	 * @param bag Tableau de Money
	 * Construit un MoneyBag en regroupant tous les Money du tableau
	 */
	MoneyBag(Money bag[]){
		for (int i = 0; i < bag.length; ++i)
			appendMoney(bag[i]);
	}
	
	/**
	 * @param m Money
	 * @param mb MoneyBag
	 * Construit un MoneyBag en regroupant m et tous les Money de mb
	 */
	MoneyBag(Money m, MoneyBag mb) {
	    appendMoney(m);
	    appendBag(mb);
	}
	
	/**
	 * 
	 * @param mb1 MoneyBag
	 * @param mb2 MoneyBag
	 * Construit un MoneyBag en regroupant tous les Money de mb1 et mb2
	 */
    MoneyBag(MoneyBag mb1, MoneyBag mb2) {
        appendBag(mb1);
        appendBag(mb2);
    }
	
	private void appendMoney(Money m) {
		if (fMonies.isEmpty())
			fMonies.add(m);
		else {
			int i = 0;
			while (i < fMonies.size() && !fMonies.get(i).getCurrency().equals(m.getCurrency()))
				i++;
				
			if(i >= fMonies.size())
				fMonies.add(m);
			else
				fMonies.set(i, new Money(fMonies.get(i).getAmount() + m.getAmount(), m.getCurrency()));
		}
	}
	
    private void appendBag(MoneyBag mb) {
    	for (int i = 0; i < mb.fMonies.size(); ++i)
    		appendMoney(mb.fMonies.get(i));
    }

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		
		if (this == obj)
			return true;
		
		if (!(obj instanceof MoneyBag))
			return false;
		
		MoneyBag other = (MoneyBag) obj;
		
		/*
		 * L'index de fMonies represente une devise. Si le nombre de devise n'est pas le meme, alors il s'agit de MoneyBag differents.
		 * Si le nombre de devises est le meme dans les deux MoneyBag, on passe � la suite, sinon on retourne false.
		 * Ce test est facultatif mais il permet un gain de temps dans le cas ou les MoneyBag ne contiennent pas les memes devises.
		 * -> On evite d'effectuer des actions supplementaires (creation de HashMap, operations sur HashMap etc...)
		 */
		if(fMonies.size() != other.fMonies.size())
			return false; 
		
		/*
		 * Le test precedent ayant reussi, les deux MoneyBag sont susceptibles d'etre les memes car ils ont le meme nombre de devises.
		 * On cree des HashMap pour mieux regrouper les devises et donc mieux les comparer.
		 * -> Les HashMap contiendront un couple de <currency, amount> (<devise, quantite>).
		 */
		HashMap<String, Integer> thisObjMap  = new HashMap<String, Integer>(); //HashMap pour l'objet courant (this)
		HashMap<String, Integer> otherObjMap = new HashMap<String, Integer>(); //HashMap pour l'objet cible (other)
		
		/*
		 * On construit les deux HashMap � partir de l'attribut fMonies de chacun des deux MoneyBag
		 * Si le HashMap contient deja un montant pour la devise, on additionne les montants. Sinon on insere simplement.
		 * La condition se verifie grace a l'operateur ternaire au lieu d'un if else pour ameliorer la vitesse d'execution.
		 * L'operateur ternaire est beaucoup plus rapide car il s'agit d'un simple decalage en memoire. 
		 */
		for(int i = 0; i < fMonies.size(); ++i) {
			thisObjMap.put(fMonies.get(i).getCurrency(), (thisObjMap.get(fMonies.get(i).getCurrency()) == null) ? fMonies.get(i).getAmount() : thisObjMap.get(fMonies.get(i).getCurrency()) + fMonies.get(i).getAmount());
			otherObjMap.put(other.fMonies.get(i).getCurrency(), (otherObjMap.get(other.fMonies.get(i).getCurrency()) == null) ? other.fMonies.get(i).getAmount() : otherObjMap.get(other.fMonies.get(i).getCurrency()) + other.fMonies.get(i).getAmount());
		}
		
		// On recupere les devises sous forme de tableau
		Set<String> thisCurrencies = thisObjMap.keySet();
		Set<String> otherCurrencies = otherObjMap.keySet();
		
		// On verifie si les deux tableaux de devises contiennent les memes devises, sinon on retourne false.
		if (!thisCurrencies.equals(otherCurrencies)) 
			return false;
		
		// On compare le montant des devises, s'il y a une difference on retourne false, sinon true.
		for (String currency : thisCurrencies)
			if (thisObjMap.get(currency).intValue() != otherObjMap.get(currency).intValue())
				return false;
				
		return true;
	}
	
	@Override
    public IMoney add(IMoney m) {
        return m.addMoneyBag(this);
	}
	
	/**
	 * @param m Money
	 * On construit un nouveau MoneyBag en additionnant le MoneyBag actuel et m
	 * Ensuite, on simplifie l'objet et on le retoune (voir consigne question 14) 
	 */
	@Override
	public IMoney addMoney(Money m) {
        return (new MoneyBag(m, this)).simplify();
	}
	
	/**
	 * @param mb MoneyBag
	 * On construit un nouveau MoneyBag en additionnant le MoneyBag actuel et mb
	 * Ensuite, on simplifie l'objet
	 */
	@Override
	public IMoney addMoneyBag(MoneyBag mb) {
        return (new MoneyBag(this, mb)).simplify();
	}
	
	/**
	 * Utile apres une soustraction
	 * On extrait les Money ayant un montant > 0 du MoneyBag actuel
	 * Ensuite, on retourne soit un MoneyBag, soit un Money si le MoneyBag n'est compose que d'un seul Money
	 * @return MoneyBag ou Money
	 */
    private IMoney simplify() {
    	ArrayList<Money> bag = new ArrayList<Money>();     	
    	
    	// On retire tous les montants inferieurs ou egal a 0
    	for(int i = 0; i < fMonies.size(); ++i) {
    		if(fMonies.get(i).getAmount() > 0)
    			bag.add(fMonies.get(i));
    	}
    	
    	Money[] tmp = new Money[bag.size()];
    	tmp = bag.toArray(tmp);
    	
    	MoneyBag mb = new MoneyBag(tmp);
    	
    	return (mb.fMonies.size() == 1) ? (IMoney) mb.fMonies.get(0) : (IMoney) mb;
	}
}

import static org.junit.Assert.*;
import org.junit.Test;

public class MoneyTest {
	private Money m12CHF = new Money(12, "CHF");
	private Money m14CHF = new Money(14, "CHF");
	
	@Test
	public void testSimpleAdd() {
		Money expected = new Money(26, "CHF");
		assertTrue(expected.equals(m12CHF.add(m14CHF)));
	}
	
	@Test
	public void testEquals(){
		assertTrue(!m12CHF.equals(null));
		assertEquals(m12CHF, m12CHF);
		assertEquals(m12CHF, new Money(12, "CHF"));
		assertTrue(!m12CHF.equals(m14CHF));
	}
}
